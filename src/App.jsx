import React, {useEffect} from "react";
import {of} from "rxjs";
import {component} from "cycle-react/rxjs";
import CountdownView from "./CountdownView";

let App = component("App", function (interactions, props) {
    // const [hour, setHour] = useState(1);
    // const [minutes, setMin] = useState(10);
    // const [seconds, setSec] = useState(0);
    //
    // function handlerForHour(e) {
    //     const val = e.target.value;
    //     setHour(val)
    // }
    //
    // function handlerForMinute(e) {
    //     const val = e.target.value;
    //     setMin(val)
    // }
    //
    // function handlerForSeconds(e) {
    //     const val = e.target.value;
    //
    //     setSec(val)
    // }
    //
    // useEffect(() => {
    //
    // }, [useState])

    return of(
        <div className="App">
            {/*<Row gutter={8} type="flex" justify="space-around">*/}
            {/*    <Col span={8}>*/}
            {/*        <Input onChange={handlerForHour}/>*/}
            {/*    </Col>*/}
            {/*    <Col span={8}>*/}
            {/*        <Input onChange={handlerForMinute}/>*/}
            {/*    </Col>*/}
            {/*    <Col span={8}>*/}
            {/*        <Input onChange={handlerForSeconds}/>*/}
            {/*    </Col>*/}
            {/*</Row>*/}
            {/*<CountdownView*/}
            {/*    hours={hour}*/}
            {/*    minutes={minutes}*/}
            {/*    seconds={seconds}/>*/}
            <CountdownView hours={1} minutes={30} seconds={0}/>
        </div>
    );
});

export default App;
